//
//  CustomError.swift
//  Matching Pairs
//
//  Created by Lavinia Tigaeru on 26.01.2023.
//

import Foundation

enum CustomError: Error {
    case invalidUrl
    case invalidData
}
