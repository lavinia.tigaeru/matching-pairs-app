//
//  Theme.swift
//  Matching Pairs
//
//  Created by Lavinia Tigaeru on 26.01.2023.
//

import Foundation
import UIKit

struct Theme: Codable {
    let cardColor: ThemeColor
    let cardSymbol: String
    let symbols: [String]
    let title: String
    
    enum CodingKeys: String, CodingKey {
        case cardColor = "card_color"
        case cardSymbol = "card_symbol"
        case symbols
        case title
    }
}

struct ThemeColor: Codable {
    let blue: Float
    let green: Float
    let red: Float
    
    func getColor() -> UIColor {
        return UIColor(red: CGFloat(red), green: CGFloat(green), blue: CGFloat(blue), alpha: 1.0)
    }
}
