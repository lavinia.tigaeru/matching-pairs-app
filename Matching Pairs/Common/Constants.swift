//
//  Constants.swift
//  Matching Pairs
//
//  Created by Lavinia Tigaeru on 26.01.2023.
//

import Foundation

struct Constants {
    static let namePrompt = "Who's playing?"
    static let nameWarning = "Add your name."
    static let statusLost = "You lost. 🥲"
    static let statusWon = "You won! 🥳"
    static let themePrompt = "Pick a theme:"
    static let themeWarning = "Pick a theme."
    static let themeUrl = "https://firebasestorage.googleapis.com/v0/b/concentrationgame-20753.appspot.com/o/themes.json?alt=media&token=6898245a-0586-4fed-b30e-5078faeba078"
}
