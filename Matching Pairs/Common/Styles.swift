//
//  Styles.swift
//  Matching Pairs
//
//  Created by Lavinia Tigaeru on 26.01.2023.
//

import Foundation
import UIKit

struct Styles {
    static let buttonFont = getFontWithSizes(iphone: 20.0, ipad: 30.0)
    static let gameTimerFont = getFontWithSizes(iphone: 35.0, ipad: 45.0)
    static let gameScoreFont = getFontWithSizes(iphone: 30.0, ipad: 40.0)
    static let gameStatusFont = getFontWithSizes(iphone: 25.0, ipad: 35.0)
    static let homepageLabelFont = getFontWithSizes(iphone: 18.0, ipad: 30.0)
    static let homepageTextfieldFont = getFontWithSizes(iphone: 15.0, ipad: 25.0)
    static let leaderboardButtonFont = getFontWithSizes(iphone: 18.0, ipad: 25.0)
    static let leaderboardEntryFont = getFontWithSizes(iphone: 20.0, ipad: 25.0)
    
    static func getFontWithSizes(iphone: CGFloat, ipad: CGFloat) -> UIFont? {
        return UIDevice.current.userInterfaceIdiom == .pad ? UIFont(name: "Arial", size: ipad) : UIFont(name: "Arial", size: iphone)
    }
}
