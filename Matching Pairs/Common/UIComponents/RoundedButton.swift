//
//  RoundedButton.swift
//  Matching Pairs
//
//  Created by Lavinia Tigaeru on 26.01.2023.
//

import UIKit

class RoundedButton: UIButton {
    override func awakeFromNib() {
        super.awakeFromNib()
        setupView()
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        setupView()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        setupView()
    }
    
    func setupView() {
        setTitleColor(.label, for: .normal)
        layer.cornerRadius = frame.size.height / 2
        layer.masksToBounds = true
        layer.borderWidth = 3
        layer.borderColor = UIColor.systemBlue.cgColor
        titleLabel?.font = Styles.buttonFont
    }
}
