//
//  LeaderboardCell.swift
//  Matching Pairs
//
//  Created by Lavinia Tigaeru on 27.01.2023.
//

import UIKit

class LeaderboardCell: UITableViewCell {
    lazy var orderLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = Styles.leaderboardEntryFont
        return label
    }()
    
    lazy var nameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = Styles.leaderboardEntryFont
        return label
    }()
    
    lazy var scoreLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = Styles.leaderboardEntryFont
        return label
    }()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.addSubview(orderLabel)
        contentView.addSubview(nameLabel)
        contentView.addSubview(scoreLabel)
        
        NSLayoutConstraint.activate([
            orderLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 4),
            orderLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -4),
            orderLabel.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 4),
            
            nameLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 4),
            nameLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -4),
            nameLabel.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 30),
            
            scoreLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 4),
            scoreLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -4),
            scoreLabel.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -4),
        ])
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    func configure(order: Int, name: String, score: Int) {
        orderLabel.text = "\(order)"
        nameLabel.text = name
        scoreLabel.text = "\(score)"
    }
}
