//
//  CardCell.swift
//  Matching Pairs
//
//  Created by Lavinia Tigaeru on 27.01.2023.
//

import UIKit

class CardCell: UICollectionViewCell {
    lazy var symbolLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = label.font.withSize(30)
        return label
    }()
    
    var flipped = false
    var matched = false
    var frontSymbol = ""
    var backSymbol = ""
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        contentView.layer.cornerRadius = 10
        contentView.addSubview(symbolLabel)
        symbolLabel.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
        symbolLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor).isActive = true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure(front: String, back: String, bgColor: UIColor, flipped: Bool) {
        frontSymbol = front
        backSymbol = back
        self.flipped = flipped
        symbolLabel.text = flipped ? front : back
        contentView.backgroundColor = bgColor
        matched = false
        contentView.isHidden = false
        isUserInteractionEnabled = true
    }
    
    func flip(completionHandler: ((Bool) -> Void)? = nil) {
        flipped = !flipped
        if flipped {
            // Show front
            symbolLabel.text = frontSymbol
            UIView.transition(with: self,
                              duration: 0.3,
                              options: .transitionFlipFromLeft,
                              animations: nil,
                              completion: completionHandler)
        } else {
            // Show back
            symbolLabel.text = backSymbol
            UIView.transition(with: self,
                              duration: 0.3,
                              options: .transitionFlipFromRight,
                              animations: nil,
                              completion: completionHandler)
        }
    }
    
    func match() {
        matched = true
        contentView.isHidden = true
        isUserInteractionEnabled = false
    }
}
