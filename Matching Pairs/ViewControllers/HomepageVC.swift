//
//  HomepageVC.swift
//  Matching Pairs
//
//  Created by Lavinia Tigaeru on 26.01.2023.
//

import UIKit

class HomepageVC: UIViewController {
    
    // MARK: UI Elements
    lazy var splashScreenImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "launchscreen.png")
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        return imageView
    }()
    
    lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.backgroundColor = .clear
        stackView.axis = .vertical
        stackView.distribution = .equalSpacing
        stackView.alignment = .center
        stackView.spacing = 15.0
        return stackView
    }()
    
    lazy var nameStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.backgroundColor = .clear
        stackView.axis = .vertical
        stackView.distribution = .fill
        stackView.alignment = .leading
        stackView.spacing = 15.0
        return stackView
    }()
    
    lazy var nameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = Constants.namePrompt
        label.font = Styles.homepageLabelFont
        label.textColor = .label
        return label
    }()
    
    lazy var nameTextField: UITextField = {
        let textField = UITextField()
        textField.textColor = .label
        textField.font = Styles.homepageTextfieldFont
        textField.keyboardType = .default
        textField.borderStyle = .roundedRect
        textField.layer.cornerRadius = 5.0
        textField.layer.borderColor = UIColor.systemBlue.cgColor
        textField.layer.borderWidth = 1.0
        textField.returnKeyType = .done
        textField.clearButtonMode = .whileEditing
        textField.delegate = self
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 10.0, height: textField.frame.size.height))
        textField.leftView = paddingView
        textField.leftViewMode = .always
        return textField
    }()
    
    lazy var themeStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.backgroundColor = .clear
        stackView.axis = .vertical
        stackView.distribution = .equalSpacing
        stackView.alignment = .leading
        stackView.spacing = 15.0
        return stackView
    }()
    
    lazy var themeLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = Constants.themePrompt
        label.font = Styles.homepageLabelFont
        label.textColor = .label
        return label
    }()
    
    lazy var pickThemeStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.backgroundColor = .clear
        stackView.axis = .vertical
        stackView.distribution = .fillEqually
        stackView.alignment = .center
        stackView.spacing = 15.0
        return stackView
    }()
    
    lazy var playButton: RoundedButton = {
        let button = RoundedButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("PLAY", for: .normal)
        button.backgroundColor = .systemBlue
        button.addTarget(self, action: #selector(playTapped), for: .touchUpInside)
        return button
    }()
    
    // MARK: Properties
    var themes = [Theme]()
    var selectedTheme: Theme? = nil
    var width: CGFloat = 0
    var height: CGFloat = 0
    var verticalMargin: CGFloat = 0
    var topAnchor: NSLayoutYAxisAnchor!
    var bottomAnchor: NSLayoutYAxisAnchor!
    var themeOptionButtons = [RoundedButton]()
    
    // MARK: Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .systemBackground
        computeDimensions()
        configureSplashScreen()
        configureView()
        loadThemes()
    }
    
    // MARK: Selectors
    @objc func playTapped() {
        if let name = nameTextField.text {
            if name.isEmpty {
                print("empty name")
                showWarning(for: Constants.nameWarning)
                return
            }
            if selectedTheme == nil {
                print("empty theme")
                showWarning(for: Constants.themeWarning)
                return
            }
            let vc = GameVC()
            vc.theme = selectedTheme
            vc.name = name
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @objc func themeTapped(sender: RoundedButton) {
        selectedTheme = themes[sender.tag]
        for btn in themeOptionButtons {
            if btn.tag == sender.tag {
                btn.backgroundColor = .systemBlue
            } else {
                btn.backgroundColor = .systemBackground
            }
        }
        print("tag: \(sender.tag) | selectedTheme: \(selectedTheme?.symbols ?? [])")
    }
    
    // MARK: Custom Methods
    func computeDimensions() {
        if view.frame.size.width < view.frame.size.height {
            width = UIDevice.current.userInterfaceIdiom == .pad ? view.frame.size.width - 120.0 : view.frame.size.width - 60
        } else {
            width = UIDevice.current.userInterfaceIdiom == .pad ? view.frame.size.height - 120.0 : view.frame.size.height - 60
        }
        height = UIDevice.current.userInterfaceIdiom == .pad ? 60.0 : 40.0
        verticalMargin = UIDevice.current.userInterfaceIdiom == .pad ? 80.0 : 30.0
        topAnchor = view.safeAreaLayoutGuide.topAnchor
        bottomAnchor = view.safeAreaLayoutGuide.bottomAnchor
    }
    
    func configureSplashScreen() {
        view.addSubview(splashScreenImageView)
        view.bringSubviewToFront(splashScreenImageView)
        
        NSLayoutConstraint.activate([
            splashScreenImageView.topAnchor.constraint(equalTo: view.topAnchor),
            splashScreenImageView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            splashScreenImageView.leftAnchor.constraint(equalTo: view.leftAnchor),
            splashScreenImageView.rightAnchor.constraint(equalTo: view.rightAnchor)
        ])
    }
    
    func configureView() {
        view.addSubview(stackView)
        view.sendSubviewToBack(stackView)
        stackView.addArrangedSubview(nameStackView)
        nameStackView.addArrangedSubview(nameLabel)
        nameStackView.addArrangedSubview(nameTextField)
        
        stackView.addArrangedSubview(themeStackView)
        themeStackView.addArrangedSubview(themeLabel)
        themeStackView.addArrangedSubview(pickThemeStackView)
        
        stackView.addArrangedSubview(playButton)
        
        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo: topAnchor),
            stackView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -verticalMargin),
            stackView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            
            nameLabel.leftAnchor.constraint(equalTo: stackView.leftAnchor),
            nameTextField.leftAnchor.constraint(equalTo: stackView.leftAnchor),
            nameTextField.heightAnchor.constraint(equalToConstant: height),
            nameTextField.widthAnchor.constraint(equalToConstant: width),

            themeLabel.leftAnchor.constraint(equalTo: stackView.leftAnchor),
            
            playButton.heightAnchor.constraint(equalToConstant: height),
            playButton.widthAnchor.constraint(equalToConstant: width)
        ])
    }
    
    func configureThemesStackView() {
        for (index, theme) in themes.enumerated() {
            let button = RoundedButton()
            button.tag = index
            button.translatesAutoresizingMaskIntoConstraints = false
            button.setTitle(theme.title + " \(theme.symbols[0])", for: .normal)
            button.addTarget(self, action: #selector(themeTapped(sender:)), for: .touchUpInside)
            pickThemeStackView.addArrangedSubview(button)
            themeOptionButtons.append(button)
            
            NSLayoutConstraint.activate([
                button.heightAnchor.constraint(equalToConstant: height),
                button.widthAnchor.constraint(equalToConstant: width)
            ])
        }
    }
    
    func loadThemes() {
        ThemeMethods.getAvailableThemes(completion: { [weak self] result in
            switch result {
            case .success(let themes):
                print(themes)
                self?.themes = themes
                DispatchQueue.main.async {
                    self?.configureThemesStackView()
                    self?.splashScreenImageView.isHidden = true
                }
            case .failure(let error):
                print(error)
            }
        })
    }
    
    func showWarning(for message: String) {
        let alert = UIAlertController(title: "", message: message, preferredStyle: .alert)
        self.present(alert, animated: true, completion: nil)

        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1) {
            alert.dismiss(animated: true, completion: nil)
        }
    }
}

extension HomepageVC: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
