//
//  GameVC.swift
//  Matching Pairs
//
//  Created by Lavinia Tigaeru on 26.01.2023.
//

import UIKit
import GameManager

class GameVC: UIViewController {
    
    // MARK: UI Elements
    lazy var timerLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = Styles.gameTimerFont
        label.text = "01:00"
        label.textColor = .label
        return label
    }()
    
    lazy var cardsCollectionView: UICollectionView = {
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.contentInsetAdjustmentBehavior = .always
        collectionView.register(CardCell.self, forCellWithReuseIdentifier: "cardCell")
        collectionView.delegate = self
        collectionView.dataSource = self
        return collectionView
    }()
    
    lazy var playButton: RoundedButton = {
        let button = RoundedButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("START", for: .normal)
        button.backgroundColor = .systemBlue
        button.addTarget(self, action: #selector(playTapped), for: .touchUpInside)
        return button
    }()
    
    lazy var statusStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.backgroundColor = .clear
        stackView.axis = .vertical
        stackView.distribution = .equalSpacing
        stackView.alignment = .center
        stackView.spacing = 10.0
        return stackView
    }()
    
    lazy var scoreLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = Styles.gameScoreFont
        label.textColor = .label
        return label
    }()
    
    lazy var statusLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = Styles.gameStatusFont
        label.textColor = .label
        return label
    }()
    
    lazy var leaderboardButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Check leaderboard >", for: .normal)
        button.setTitleColor(.systemBlue, for: .normal)
        button.backgroundColor = .clear
        button.titleLabel?.font = Styles.leaderboardButtonFont
        button.addTarget(self, action: #selector(leaderboardTapped), for: .touchUpInside)
        return button
    }()
    
    // MARK: Properties
    // UI related
    var buttonWidth: CGFloat = 0
    var buttonHeight: CGFloat = 0
    var topMargin: CGFloat = 0
    var verticalMargin: CGFloat = 0
    var cellsPerRow: CGFloat = 0
    var cellsPerColumn: CGFloat = 0
    let inset: CGFloat = 10
    let minimumLineSpacing: CGFloat = 10
    let minimumInteritemSpacing: CGFloat = 10
    var topAnchor: NSLayoutYAxisAnchor!
    var bottomAnchor: NSLayoutYAxisAnchor!
    let layout = UICollectionViewFlowLayout()
    // Game related
    var name = ""
    var theme: Theme!
    var cardValues = [String]()
    var isPlaying = false
    var gameFinished = false
    var flipsCounter = 0
    var cardsMatchedCounter = 0 {
        didSet {
            print("cardsMatchedCounter: \(cardsMatchedCounter)")
            if cardsMatchedCounter == cardValues.count {
                endGame(won: true)
            }
        }
    }
    var lastFlippedIndex = IndexPath()
    let secondsPerGame = 60.0
    var secondsLeft = 60.0
    weak var timer: Timer?
    
    // MARK: Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .systemBackground
        cardValues = GameManager.generateCardSet(from: theme.symbols)
        computeStaticDimensions()
        computeDimensions(screenWidth: view.frame.size.width, screenHeight: view.frame.size.height)
        configureView()
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        computeDimensions(screenWidth: size.width, screenHeight: size.height)
        layout.invalidateLayout()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        timer?.invalidate()
    }
    
    // MARK: Selectors
    @objc func playTapped() {
        if gameFinished {
            gameFinished = false
            resetGame()
            isPlaying = false
            statusStackView.isHidden = true
            scoreLabel.isHidden = false
            cardsCollectionView.isHidden = false
            return
        }
        if isPlaying {
            resetGame()
        } else {
            playButton.setTitle("RESET", for: .normal)
            isPlaying = !isPlaying
            if timer == nil {
                timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTimer), userInfo: nil, repeats: true)
            }
            cardsCollectionView.reloadData()
        }
    }
    
    @objc func updateTimer() {
        secondsLeft -= 1
        timerLabel.text = timeString(time: TimeInterval(secondsLeft))
        if secondsLeft == 0 {
            timer?.invalidate()
            print("you lost!")
            endGame(won: false)
        }
    }
    
    @objc func leaderboardTapped() {
        let vc = LeaderboardVC()
        vc.scores = ScoreHandler.getAllScores(for: theme.title)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    // MARK: Custom Methods
    func timeString(time: TimeInterval) -> String {
        let minutes = Int(time) / 60 % 60
        let seconds = Int(time) % 60
        return String(format: "%02i:%02i", minutes, seconds)
    }
    
    func computeStaticDimensions() {
        buttonHeight = UIDevice.current.userInterfaceIdiom == .pad ? 60.0 : 45.0
        topMargin = UIDevice.current.userInterfaceIdiom == .pad ? 30.0 : 5.0
        verticalMargin = UIDevice.current.userInterfaceIdiom == .pad ? 80.0 : 20.0
        topAnchor = view.topAnchor
        bottomAnchor = view.bottomAnchor
        if #available(iOS 11.0, *) {
            topAnchor = view.safeAreaLayoutGuide.topAnchor
            bottomAnchor = view.safeAreaLayoutGuide.bottomAnchor
        }
    }
    
    func computeDimensions(screenWidth: Double, screenHeight: Double) {
        if screenWidth > screenHeight {
            buttonWidth = screenHeight - 100
            cellsPerRow = UIDevice.current.userInterfaceIdiom == .pad ? 8 : 6
            cellsPerColumn = ceil(CGFloat(cardValues.count) / cellsPerRow)
        } else {
            buttonWidth = screenWidth - 100
            cellsPerRow = UIDevice.current.userInterfaceIdiom == .pad ? 4 : 3
            cellsPerColumn = ceil(CGFloat(cardValues.count) / cellsPerRow)
        }
    }
    
    func configureView() {
        view.addSubview(statusStackView)
        statusStackView.addArrangedSubview(scoreLabel)
        statusStackView.addArrangedSubview(statusLabel)
        statusStackView.addArrangedSubview(leaderboardButton)
        statusStackView.isHidden = true
        view.addSubview(timerLabel)
        view.addSubview(cardsCollectionView)
        view.addSubview(playButton)
        
        NSLayoutConstraint.activate([
            timerLabel.topAnchor.constraint(equalTo: topAnchor, constant: topMargin),
            timerLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            
            cardsCollectionView.topAnchor.constraint(equalTo: timerLabel.bottomAnchor, constant: verticalMargin),
            cardsCollectionView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: verticalMargin),
            cardsCollectionView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -verticalMargin),
            cardsCollectionView.bottomAnchor.constraint(equalTo: playButton.topAnchor, constant: -verticalMargin),
            
            statusStackView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            statusStackView.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            
            leaderboardButton.heightAnchor.constraint(equalToConstant: buttonHeight),
            leaderboardButton.widthAnchor.constraint(equalToConstant: buttonWidth),
            
            playButton.heightAnchor.constraint(equalToConstant: buttonHeight),
            playButton.widthAnchor.constraint(equalToConstant: buttonWidth),
            playButton.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -verticalMargin),
            playButton.centerXAnchor.constraint(equalTo: view.centerXAnchor)
        ])
    }
    
    func resetGame() {
        playButton.setTitle("START", for: .normal)
        isPlaying = !isPlaying
        flipsCounter = 0
        cardsMatchedCounter = 0
        lastFlippedIndex = IndexPath()
        // Reset timer
        timer?.invalidate()
        secondsLeft = secondsPerGame
        timerLabel.text = timeString(time: TimeInterval(secondsLeft))
        // Refresh cards
        cardValues.shuffle()
        cardsCollectionView.reloadData()
    }
    
    func endGame(won: Bool) {
        if won {
            statusLabel.text = Constants.statusWon
            let currentScore = GameManager.computeScore(movesCount: flipsCounter,
                                                        cardsCount: cardValues.count,
                                                        timeLeft: Int(secondsLeft))
            // Save score locally
            let possibleSavedScore = ScoreHandler.getScoreFor(name: name, theme: theme.title)
            if possibleSavedScore.isEmpty {
                // This user won for the first time
                print("This user won for the first time")
                ScoreHandler.saveScore(currentScore, for: name, theme: theme.title)
                scoreLabel.text = "SCORE: \(currentScore)"
            } else {
                // This user played before
                let oldScore = possibleSavedScore[0].value
                if currentScore > oldScore {
                    // New best score
                    print("New best score")
                    ScoreHandler.updateScore(possibleSavedScore[0], newValue: currentScore)
                    scoreLabel.text = "NEW BEST SCORE: \(currentScore)"
                } else {
                    print("Lower score, don't save")
                    scoreLabel.text = "SCORE: \(currentScore)"
                }
            }
        } else {
            statusLabel.text = Constants.statusLost
            scoreLabel.isHidden = true
        }
        timer?.invalidate()
        isPlaying = false
        playButton.setTitle("TRY AGAIN", for: .normal)
        gameFinished = true
        cardsCollectionView.isHidden = true
        statusStackView.isHidden = false
    }
}

extension GameVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return cardValues.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = cardsCollectionView.dequeueReusableCell(withReuseIdentifier: "cardCell", for: indexPath) as! CardCell
        cell.configure(front: cardValues[indexPath.item],
                       back: theme.cardSymbol,
                       bgColor: theme.cardColor.getColor(),
                       flipped: !isPlaying)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return minimumLineSpacing
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return minimumInteritemSpacing
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let marginsAndInsetsW = inset * 2 + cardsCollectionView.safeAreaInsets.left + cardsCollectionView.safeAreaInsets.right + minimumInteritemSpacing * CGFloat(cellsPerRow - 1)
        let marginsAndInsetsH = inset * 2 + cardsCollectionView.safeAreaInsets.top + cardsCollectionView.safeAreaInsets.bottom + minimumInteritemSpacing * CGFloat(cellsPerColumn - 1)
        
        let itemWidth = ((cardsCollectionView.bounds.size.width - marginsAndInsetsW) / CGFloat(cellsPerRow)).rounded(.down)
        let itemHeight = ((cardsCollectionView.bounds.size.height - marginsAndInsetsH) / CGFloat(cellsPerColumn)).rounded(.down)
        return CGSize(width: itemWidth, height: itemHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if isPlaying {
            if indexPath != lastFlippedIndex {
                flipsCounter += 1
            }
            print("flips: \(flipsCounter)")
            let currentCard = collectionView.cellForItem(at: indexPath) as! CardCell
            currentCard.flip(completionHandler: { [weak self] _ in
                guard let self = self else { return }
                // We have two flipped cards
                if self.flipsCounter % 2 == 0 {
                    let previousCard = collectionView.cellForItem(at: self.lastFlippedIndex) as! CardCell
                    if currentCard.frontSymbol == previousCard.frontSymbol {
                        // We have a match
                        previousCard.match()
                        currentCard.match()
                        self.cardsMatchedCounter += 2
                    } else {
                        // We don't have a match
                        previousCard.flip()
                        currentCard.flip()
                    }
                }
                self.lastFlippedIndex = indexPath
            })
        } else {
            print("Press START first!")
        }
    }
}
