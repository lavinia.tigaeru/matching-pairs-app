//
//  LeaderboardVC.swift
//  Matching Pairs
//
//  Created by Lavinia Tigaeru on 27.01.2023.
//

import UIKit

class LeaderboardVC: UIViewController {

    // MARK: UI Elements
    lazy var leaderboardImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "leaderboard.png")
        imageView.contentMode = .scaleAspectFit
        imageView.clipsToBounds = true
        return imageView
    }()
    
    lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(LeaderboardCell.self, forCellReuseIdentifier: "leaderboardCell")
        tableView.rowHeight = UITableView.automaticDimension
        tableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 0.001))
        return tableView
    }()
    
    // MARK: Properties
    var imageWidth: CGFloat = 0
    var imageHeight: CGFloat = 0
    var verticalMargin: CGFloat = 0
    var horizontalMargin: CGFloat = 0
    var topAnchor: NSLayoutYAxisAnchor!
    var bottomAnchor: NSLayoutYAxisAnchor!
    var scores = [Score]()
    
    // MARK: Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .systemBackground
        computeDimensions()
        configureView()
    }
    
    // MARK: Custom Methods
    func computeDimensions() {
        if view.frame.size.width < view.frame.size.height {
            imageWidth = UIDevice.current.userInterfaceIdiom == .pad ? view.frame.size.width - 400 : view.frame.size.width - 200
        } else {
            imageWidth = UIDevice.current.userInterfaceIdiom == .pad ? view.frame.size.height - 400 : view.frame.size.height - 200
        }
        // Image dimensions: 1261 × 675
        imageHeight = imageWidth * 675 / 1261
        verticalMargin = UIDevice.current.userInterfaceIdiom == .pad ? 30.0 : 10.0
        horizontalMargin = UIDevice.current.userInterfaceIdiom == .pad ? 60.0 : 20.0
        topAnchor = view.topAnchor
        bottomAnchor = view.bottomAnchor
        if #available(iOS 11.0, *) {
            topAnchor = view.safeAreaLayoutGuide.topAnchor
            bottomAnchor = view.safeAreaLayoutGuide.bottomAnchor
        }
    }
    
    func configureView() {
        view.addSubview(leaderboardImageView)
        view.addSubview(tableView)
        
        NSLayoutConstraint.activate([
            leaderboardImageView.topAnchor.constraint(equalTo: topAnchor),
            leaderboardImageView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            leaderboardImageView.heightAnchor.constraint(equalToConstant: imageHeight),
            leaderboardImageView.widthAnchor.constraint(equalToConstant: imageWidth),
            
            tableView.topAnchor.constraint(equalTo: leaderboardImageView.bottomAnchor, constant: verticalMargin),
            tableView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -verticalMargin),
            tableView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: horizontalMargin),
            tableView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -horizontalMargin)
        ])
    }
}

extension LeaderboardVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return scores.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "leaderboardCell") as! LeaderboardCell
        let i = indexPath.row
        cell.configure(order: i + 1, name: scores[i].name ?? "", score: Int(scores[i].value))
        return cell
    }
}
