//
//  ThemeMethods.swift
//  Matching Pairs
//
//  Created by Lavinia Tigaeru on 26.01.2023.
//

import Foundation
import Alamofire

class ThemeMethods {
    static func getAvailableThemes(completion: @escaping (Result<[Theme], Error>) -> ()) {
        AF.request(
            Constants.themeUrl,
            method: .get)
            .validate()
            .response { response in
                switch response.result {
                case .success(let data):
                    guard let data = data else {
                        completion(.failure(CustomError.invalidData))
                        return
                    }
                    
                    do {
                        let themes = try JSONDecoder().decode([Theme].self, from: data)
                        completion(.success(themes))
                    } catch {
                        completion(.failure(error))
                    }
                case .failure(let error):
                    completion(.failure(error))
                }
            }
    }
}
