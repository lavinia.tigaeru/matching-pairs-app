//
//  Score+CoreDataProperties.swift
//  Matching Pairs
//
//  Created by Lavinia Tigaeru on 27.01.2023.
//
//

import Foundation
import CoreData


extension Score {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Score> {
        return NSFetchRequest<Score>(entityName: "Score")
    }

    @NSManaged public var name: String?
    @NSManaged public var value: Int16
    @NSManaged public var theme: String?

}

extension Score : Identifiable {

}
