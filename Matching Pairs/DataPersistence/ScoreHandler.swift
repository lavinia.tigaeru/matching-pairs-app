//
//  ScoreHandler.swift
//  Matching Pairs
//
//  Created by Lavinia Tigaeru on 27.01.2023.
//

import Foundation
import CoreData
import UIKit

class ScoreHandler {
    private class func getContext() -> NSManagedObjectContext {
        let appDelegate =
            UIApplication.shared.delegate as! AppDelegate
        return appDelegate.persistentContainer.viewContext
    }
    
    class func saveScore(_ value: Int, for name: String, theme: String) {
        let context = getContext()
        context.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy
        let score = Score(context: context)
        score.value = Int16(value)
        score.name = name
        score.theme = theme
        do {
            try context.save()
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    class func deleteScore(_ score: Score) {
        let context = getContext()
        context.delete(score)
        do {
            try context.save()
        } catch let error as NSError {
            print("Could not delete. \(error), \(error.userInfo)")
        }
    }
    
    class func updateScore(_ score: Score, newValue: Int) {
        let context = getContext()
        score.value = Int16(newValue)
        do {
            try context.save()
        } catch let error as NSError {
            print("Could not update. \(error), \(error.userInfo)")
        }
    }
    
    class func getScoreFor(name: String, theme: String) -> [Score] {
        let context = getContext()
        let request = Score.fetchRequest()
        let namePredicate = NSPredicate(format: "name == %@", name)
        let themePredicate = NSPredicate(format: "theme == %@", theme)
        let andPredicate = NSCompoundPredicate(type: .and, subpredicates: [namePredicate, themePredicate])
        request.predicate = andPredicate
        do {
            let score = try context.fetch(request)
            return score
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
            return []
        }
    }
    
    class func getAllScores(for theme: String) -> [Score] {
        let context = getContext()
        let request = Score.fetchRequest()
        let themePredicate = NSPredicate(format: "theme == %@", theme)
        request.predicate = themePredicate
        do {
            var scores = try context.fetch(request)
            scores.sort { $0.value > $1.value }
            return scores
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
            return []
        }
    }
}
